# README #

Vagrant files for a simple Salt master/minion test environment

### How do I get set up? ###

See:

- http://acaird.github.io/computers/2014/09/30/salt-vagrant/
- http://humankeyboard.com/saltstack/2014/saltstack-virtualbox-vagrant.html
   - especially:
     - `VBoxManage hostonlyif create ipconfig vboxnet0 --ip 192.168.56.1 --netmask 255.255.255.0`
     - `sudo salt-key --gen-keys=minion` on the master
     - `cp minion.p* /vagrant` to copy them to your host
