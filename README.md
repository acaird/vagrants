# README #

Some Vagrantfiles for doing things I feel like doing.

### What is this repository for? ###

* Vagrantfiles
* Their Notes

### How do I get set up? ###

See http://acaird.github.io/computers/2014/09/30/salt-vagrant/

### Contribution guidelines ###

* I'm happy to look at any pull requests; each Vagrantfile should be
  in its own directory, with an optional README file, but nothing
  besides those two files.

### Who do I talk to? ###

* acaird@gmail.com
